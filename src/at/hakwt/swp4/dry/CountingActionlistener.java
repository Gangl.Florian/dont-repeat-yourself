package at.hakwt.swp4.dry;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CountingActionlistener implements ActionListener {

    private int count;


    @Override
    public void actionPerformed(ActionEvent e)
    {
        count = count + 100;
    }

    public int getCount()
    {
        return count;
    }
}
